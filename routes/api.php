<?php

use App\Http\Controllers\Buyer\BuyerCategoryController;
use App\Http\Controllers\Buyer\BuyerSellerController;
use App\Http\Controllers\Buyer\BuyerTransactionController;
use App\Http\Controllers\Category\CategoriesController;
use App\Http\Controllers\Category\CategoryBuyerController;
use App\Http\Controllers\Category\CategoryProductController;
use App\Http\Controllers\Category\CategorySellerController;
use App\Http\Controllers\Category\CategoryTransactionController;
use App\Http\Controllers\Product\ProductBuyerController;
use App\Http\Controllers\Product\ProductBuyerTransactionController;
use App\Http\Controllers\Product\ProductCategoryController;
use App\Http\Controllers\Product\ProductsController;
use App\Http\Controllers\Product\ProductTransactionController;
use App\Http\Controllers\Transaction\TransactionCategoryController;
use App\Http\Controllers\Transaction\TransactionsController;
use App\Http\Controllers\Transaction\TransactionSellerController;
use App\Http\Controllers\User\UsersController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('buyers',\App\Http\Controllers\Buyer\BuyersController::class)->only('index','show');
Route::resource('sellers',\App\Http\Controllers\Seller\SellersController::class)->only('index','show');

Route::resource('sellers.transactions',\App\Http\Controllers\Seller\SellerTransactionController::class)->only('index');
Route::resource('sellers.categories',\App\Http\Controllers\Seller\SellerCategoryController::class)->only('index');
Route::resource('sellers.buyers',\App\Http\Controllers\Seller\SellerBuyerController::class)->only('index');
Route::resource('sellers.products',\App\Http\Controllers\Seller\SellerProductController::class)->except('edit','create','show');


Route::resource('users',UsersController::class)->except('edit','create');
Route::get('users/verify/{token}',[UsersController::class,'verify'])->name('users.verify');
Route::get('users/{user}/resend-verification-email',[UsersController::class,'resend'])->name('users.resend');

Route::resource('categories',CategoriesController::class)->except('edit','create');
Route::resource('categories.products',CategoryProductController::class)->only('index');
Route::resource('categories.sellers',CategorySellerController::class)->only('index');
Route::resource('categories.transactions',CategoryTransactionController::class)->only('index');
Route::resource('categories.buyers',CategoryBuyerController::class)->only('index');


Route::resource('products',ProductsController::class)->only('index','show');
Route::resource('products.buyers',ProductBuyerController::class)->only('index');
Route::resource('products.categories',ProductCategoryController::class)->only('index','update','destroy');
Route::resource('products.transactions',ProductTransactionController::class)->only('index');
Route::resource('products.buyers.transactions',ProductBuyerTransactionController::class)->only('store');


Route::resource('transactions',TransactionsController::class)->only('index','show');

Route::resource('transactions.categories',TransactionCategoryController::class)->only('index');
Route::resource('transactions.sellers',TransactionSellerController::class)->only('index');

Route::resource('buyers.transactions',BuyerTransactionController::class)->only('index');
Route::resource('buyers.products',BuyerProductController::class)->only('index');
Route::resource('buyers.sellers',BuyerSellerController::class)->only('index');
Route::resource('buyers.categories',BuyerCategoryController::class)->only('index');

\Laravel\Passport\Passport::routes();