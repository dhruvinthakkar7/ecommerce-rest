<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CacheResponser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $url = request()->url();
        $queryParameters = request()->query();
        $method = request()->getMethod();

        ksort($queryParameters);

        $queryString = http_build_query($queryParameters);

        $fullURL = "$method:{$url}?{$queryString}";
        if(Cache::has($fullURL)){
            return new Response(Cache::get($fullURL));
        }
        return $next($request);
    }
}
