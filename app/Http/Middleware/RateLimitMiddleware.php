<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiController;
use Closure;
use Illuminate\Http\Request;

class RateLimitMiddleware extends ApiController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response =  $next($request);
        if($response->headers->get('X-RateLimit-Remaining')==0)
        {
            return $this->errorResponse('Requests limit exceeded!',429);
        }
        return $response;
    }
}
