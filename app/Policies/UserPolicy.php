<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(User $user,$ability)
    {
        if($user->isAdmin()){
            return true;
        }
    }


    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $authenticatedUser, User $user)
    {
        //
        return  $authenticatedUser->id === $user->id;
    }
    public function update(User $authenticatedUser, User $user)
    {
        //
        return  $authenticatedUser->id === $user->id;
    }


    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $authenticatedUser, User $user)
    {
        //
        return  $authenticatedUser->id === $user->id;
    }

}
